using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Permissions.Controllers;
using Permissions.Data;
using Permissions.Dtos;
using Permissions.Helpers;
using Permissions.Models;
using Permissions.Repository;
using Permissions.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Tests
{
    public class Tests
    {
        private Mock<ILogger<PermissionController>> logger;
        private Mock<IPermissionInterface> permissionRepository;
        private IMapper mapper;
        private List<Permission> permissions;
        private PermissionType permissionType;

        [SetUp]
        public void Setup()
        {
            logger = new Mock<ILogger<PermissionController>>();
            permissionRepository = new Mock<IPermissionInterface>();
            var config = new MapperConfiguration(opts =>
            {
                opts.CreateMap<Permission, PermissionToListDto>()
                .ForMember(dest => dest.DescripcionPermiso, opt => opt.MapFrom(src => src.TipoPermiso.Descripcion))
                .ForMember(dest => dest.FechaPermiso, opt => opt.MapFrom(src => src.FechaPermiso.Day + "/" + src.FechaPermiso.Month + "/" + src.FechaPermiso.Year));
            });
            mapper = config.CreateMapper();

            permissionType = new PermissionType() { Id = 1, Descripcion = "Salud" };

            permissions = new List<Permission>();
            permissions.Add(new Permission() { Id = 1, NombreEmpleado = "Cathe", ApellidoEmpleado = "Rapport", TipoPermisoId = 1, FechaPermiso = DateTime.Parse("2023-03-21 00:00:00.0000000") });
            permissions.Add(new Permission() { Id = 2, NombreEmpleado = "Raleigh", ApellidoEmpleado = "Tregent", TipoPermisoId = 1, FechaPermiso = DateTime.Parse("2023-03-02 00:00:00.0000000") });
            permissions.Add(new Permission() { Id = 3, NombreEmpleado = "Tiffany", ApellidoEmpleado = "Blackaller", TipoPermisoId = 1, FechaPermiso = DateTime.Parse("2023-03-23 00:00:00.0000000") });
            permissions.Add(new Permission() { Id = 4, NombreEmpleado = "Becca", ApellidoEmpleado = "Askie", TipoPermisoId = 1, FechaPermiso = DateTime.Parse("2023-04-06 00:00:00.0000000") });
            permissions.Add(new Permission() { Id = 5, NombreEmpleado = "Carlye", ApellidoEmpleado = "Gadie", TipoPermisoId = 1, FechaPermiso = DateTime.Parse("2023-03-25 00:00:00.0000000") });
        }

        [Test]
        public void Test1()
        {
            permissionRepository.Setup(a => a.GetPermissionList().Result).Returns(permissions);

            var permissionBL = new PermissionController(logger.Object, permissionRepository.Object, mapper);
            var actionResult = permissionBL.GetPermissions().Result;
            var okResult = actionResult as OkObjectResult;
            var permissionList = okResult.Value as List<PermissionToListDto>;

            Assert.IsTrue(permissionList.Count == 5);
        }

        [Test]
        public void Test2()
        {
            permissionRepository.Setup(a => a.GetPermissionById(2).Result).Returns(permissions[1]);

            var permissionBL = new PermissionController(logger.Object, permissionRepository.Object, mapper);
            var actionResult = permissionBL.RequestPermission(2).Result;
            var okResult = actionResult as OkObjectResult;
            var permissionList = okResult.Value as PermissionToListDto;

            Assert.IsTrue(permissionList.Id == 2);
            Assert.IsTrue(permissionList.NombreEmpleado == "Raleigh");
            Assert.IsTrue(permissionList.ApellidoEmpleado == "Tregent");
            Assert.IsTrue(permissionList.TipoPermisoId == 1);
            Assert.IsTrue(permissionList.FechaPermiso == "2/3/2023");
        }

        [Test]
        public void Test3()
        {
            var newPermission = new Permission()
            {
                Id = 4,
                NombreEmpleado = "Nombre",
                ApellidoEmpleado = "Modificado",
                TipoPermisoId = 1,
                FechaPermiso = DateTime.Parse("2023-03-26 00:00:00.0000000")
            };
            permissionRepository.Setup(a => a.UpdatePermission(newPermission).Result).Returns(newPermission);
            permissionRepository.Setup(a => a.GetPermissionById(4).Result).Returns(newPermission);

            var permissionBL = new PermissionController(logger.Object, permissionRepository.Object, mapper);
            var actionResult = permissionBL.ModifyPermission(newPermission).Result;
            var okResult = actionResult as OkObjectResult;
            var permissionList = okResult.Value as PermissionToListDto;

            Assert.IsTrue(permissionList.Id == 4);
            Assert.IsTrue(permissionList.NombreEmpleado == "Nombre");
            Assert.IsTrue(permissionList.ApellidoEmpleado == "Modificado");
            Assert.IsTrue(permissionList.TipoPermisoId == 1);
            Assert.IsTrue(permissionList.FechaPermiso == "26/3/2023");
        }
    }
}