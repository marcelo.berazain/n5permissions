// import * as React from 'react';
import PermissionList from './components/PermissionList.js';
import AboutMe from './components/AboutMe.js';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Link from '@mui/material/Link';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import './App.css';

function App() {

  return (
    <div className="App">
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" component="div">
              <Link href="/" sx={{ color: "white", padding: "30px" }}>App</Link>
            </Typography>
            <Typography variant="h6" component="div">
              <Link href="/aboutme" sx={{ color: "white" }}>Adrian Berazain</Link>
            </Typography>
          </Toolbar>
        </AppBar>
      </Box>

      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PermissionList />} />
          <Route path="/aboutme" element={<AboutMe />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
