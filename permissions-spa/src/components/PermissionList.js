import React from 'react';
import axios from 'axios';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import { FormControl, FormLabel } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';

export default class PermissionList extends React.Component {
  state = {
    permissionsList: [],
    permissionTypes: [],
    openModal: false,
    permission: null
  }

  componentDidMount() {
    axios.get(`http://localhost:5000/api/Permission`)
      .then(res => {
        const permissionsList = res.data;
        this.setState({ permissionsList });
      })
    axios.get(`http://localhost:5000/api/PermissionType`)
      .then(res => {
        const permissionTypes = res.data;
        this.setState({ permissionTypes });
      })
  }

  handleClose = () => {
    this.setState({
      openModal: false
    });
  }

  handleOpen = (object) => {
    this.setState({
      openModal: true
    });
    this.setState({
      permission: object
    });
  }

  handleNameChange = (event) => {
    const permissionEdit = this.state.permission;
    permissionEdit.nombreEmpleado = event.target.value;
    this.setState({
      permission: permissionEdit
    });
  };

  handleLastNameChange = (event) => {
    const permissionEdit = this.state.permission;
    permissionEdit.apellidoEmpleado = event.target.value;
    this.setState({
      permission: permissionEdit
    });
  };

  handleTypeChange = (event) => {
    const permissionEdit = this.state.permission;
    permissionEdit.tipoPermisoId = event.target.value;
    this.setState({
      permission: permissionEdit
    });
  };

  handleDateChange = (event) => {
    const permissionEdit = this.state.permission;
    permissionEdit.fechaPermiso = event.target.value;
    this.setState({
      permission: permissionEdit
    });
  };

  updatePermission = () => {
    var [day, monthIndex, year] = this.state.permission.fechaPermiso.split('/');
    monthIndex = monthIndex - 1;
    console.log(this.state.permission);
    axios.put('http://localhost:5000/api/Permission', {
      Id: this.state.permission.id,
      NombreEmpleado: this.state.permission.nombreEmpleado,
      ApellidoEmpleado: this.state.permission.apellidoEmpleado,
      TipoPermisoId: this.state.permission.tipoPermisoId,
      FechaPermiso: new Date(year, monthIndex, day)
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  render() {
    const style = {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      width: 400,
      bgcolor: 'background.paper',
      border: '2px solid #000',
      boxShadow: 24,
      p: 4,
    };

    return (
      <div>
        <Typography variant="h2" component="h2">
          Lista de Permisos
        </Typography>
        <TableContainer sx={{ width: "90%", marginLeft: "50px", marginRight: "50px", marginTop: "50px", marginBottom: "50px" }} component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Nombre(s)</TableCell>
                <TableCell align="center">Apellidos</TableCell>
                <TableCell align="center">Tipo de permiso</TableCell>
                <TableCell align="center">Fecha</TableCell>
                <TableCell align="center">Acciones</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.permissionsList
                .map(permission => (
                  <TableRow
                    key={permission.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align="center">{permission.nombreEmpleado}</TableCell>
                    <TableCell align="center">{permission.apellidoEmpleado}</TableCell>
                    <TableCell align="center">{permission.descripcionPermiso}</TableCell>
                    <TableCell align="center">{permission.fechaPermiso}</TableCell>
                    <TableCell align="center">
                      <Button variant="contained" onClick={() => this.handleOpen(permission)}>Editar</Button>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>

        <Modal
          open={this.state.openModal}
          onClose={this.handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          {this.state.permission != null ?
            <Box sx={style}>
              <Typography id="modal-modal-title" variant="h3" component="h2">
                Editar permiso
              </Typography>
              <FormControl sx={{ width: "100%" }}>
                <FormLabel>Nombres</FormLabel>
                <TextField
                  value={this.state.permission.nombreEmpleado}
                  onChange={this.handleNameChange}></TextField>
                <FormLabel>Apellidos</FormLabel>
                <TextField
                  value={this.state.permission.apellidoEmpleado}
                  onChange={this.handleLastNameChange}></TextField>
                <FormLabel>Tipo de permiso</FormLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={this.state.permission.tipoPermisoId}
                  label="Tipo de Permiso"
                  onChange={this.handleTypeChange}
                >
                  {this.state.permissionTypes.map(permissionType => (
                    <MenuItem value={permissionType.id}>{permissionType.descripcion}</MenuItem>
                  ))}
                </Select>
                <FormLabel>Fecha</FormLabel>
                <TextField
                  value={this.state.permission.fechaPermiso}
                  onChange={this.handleDateChange}></TextField>
                <Button variant="contained" onClick={this.updatePermission}>Modificar</Button>
              </FormControl>
            </Box> :
            <Typography id="modal-modal-title" variant="h3" component="h2">
              Cargando...
            </Typography>
          }
        </Modal>
      </div>
    )
  }
}

