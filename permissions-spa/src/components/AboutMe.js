import React from 'react';
import Typography from '@mui/material/Typography';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';

export default class AboutMe extends React.Component {
  state = {
    permissionsList: [],
    permissionTypes: [],
    openModal: false,
    permission: null
  }

  render() {
    return (
      <div>
        <Typography variant="h2" component="h2">
          Datos del desarrollador
        </Typography>
        <TableContainer sx={{ width: "50%", marginLeft: "50px", marginRight: "50px", marginTop: "50px", marginBottom: "50px" }} component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableBody>
              <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell align="left">Compañia:</TableCell>
                <TableCell align="left">N5 Technologies Inc.</TableCell>
              </TableRow>
              <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell align="left">Posición:</TableCell>
                <TableCell align="left">Desarrollador Fullstack</TableCell>
              </TableRow>
              <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell align="left">Nombre:</TableCell>
                <TableCell align="left">Adrian Marcelo Berazaín Mallea</TableCell>
              </TableRow>
              <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell align="left">Seguimiento de tareas:</TableCell>
                <TableCell align="left">
                  <Link href="https://www.notion.so/71b0c7502dd449a69d4f96afbe66adb6?v=89e5cd49296a413184e6682e0122cd63&pvs=4">Notion</Link>
                </TableCell>
              </TableRow>
              <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell align="left">Repositorio GIT:</TableCell>
                <TableCell align="left">
                  <Link href="https://gitlab.com/marcelo.berazain/n5permissions">Gitlab</Link>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
  }
}

