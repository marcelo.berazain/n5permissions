namespace Permissions.Data
{
    public interface ISeeder
    {
        void FakeData();
    }
}