using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Permissions.Models;
using Permissions.Repository.Interfaces;

namespace Permissions.Data
{
    public class Seeder : ISeeder
    {
        private readonly DataContext _context;
        private readonly IConfiguration _config;
        private readonly IPermissionTypeInterface _permissionTypeRepo;
        public Seeder(
            DataContext context,
            IConfiguration config,
            IPermissionTypeInterface permissionTypeRepo
        )
        {
            _context = context;
            _config = config;
            _permissionTypeRepo = permissionTypeRepo;
        }

        public void FakeData()
        {
            if (_config.GetSection("DatabaseParams:SeedFakeData").Value == "true" && _permissionTypeRepo.GetPermissionTypeList() == 0)
            {
                SeedFakePermissionTypes();
                SeedFakePermissions();
            }
        }

        public void SeedFakePermissionTypes()
        {
            var permissionTypeData = System.IO.File.ReadAllText("Data/SeedFakeData/permissionTypeSeedData.json");
            var permissionTypes = JsonConvert.DeserializeObject<List<PermissionType>>(permissionTypeData);
            if (permissionTypes != null)
            {
                foreach (var permissionType in permissionTypes)
                {
                    _context.PermissionTypes.Add(permissionType);
                }
                _context.SaveChanges();
            }
        }

        public void SeedFakePermissions()
        {
            var permissionData = System.IO.File.ReadAllText("Data/SeedFakeData/ProcedureSeedData.json");
            var permissions = JsonConvert.DeserializeObject<List<Permission>>(permissionData);
            if (permissions != null)
            {
                foreach (var permission in permissions)
                {
                    _context.Permissions.Add(permission);
                }
                _context.SaveChanges();
            }
        }
    }
}
