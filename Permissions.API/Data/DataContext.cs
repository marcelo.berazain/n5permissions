using Microsoft.EntityFrameworkCore;
using Permissions.Models;

namespace Permissions.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) :
            base(options)
        {
        }

        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PermissionType> PermissionTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .Entity<PermissionType>()
                .HasMany(p => p.Permisos)
                .WithOne(pr => pr.TipoPermiso)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Permission>()
                .Property(t => t.NombreEmpleado)
                .IsRequired();

            builder.Entity<Permission>()
                .Property(t => t.ApellidoEmpleado)
                .IsRequired();

            builder.Entity<Permission>()
                .Property(t => t.FechaPermiso)
                .IsRequired();

            builder.Entity<PermissionType>()
                .Property(t => t.Descripcion)
                .IsRequired();
        }

    }
}