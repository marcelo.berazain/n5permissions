using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Permissions.Repository.Interfaces;
using Permissions.Data;
using Permissions.Models;

namespace Permissions.Repository
{
    public class PermissionRepository : IPermissionInterface
    {
        private readonly DataContext _context;
        public PermissionRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Permission> GetPermissionById(int permissionId)
        {
            var permission = await _context.Permissions
                .Include(p => p.TipoPermiso)
                .FirstOrDefaultAsync(p => p.Id == permissionId);

            return permission;
        }

        public async Task<List<Permission>> GetPermissionList()
        {
            var permissions = await _context.Permissions
                .Include(p => p.TipoPermiso)
                .ToListAsync();

            return permissions;
        }

        public async Task<Permission> UpdatePermission(Permission PermissionToUpdate)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var newPermission = await _context.Permissions
                        .FirstOrDefaultAsync(p => p.Id == PermissionToUpdate.Id);

                    newPermission.NombreEmpleado = PermissionToUpdate.NombreEmpleado != null ? PermissionToUpdate.NombreEmpleado : newPermission.NombreEmpleado;
                    newPermission.ApellidoEmpleado = PermissionToUpdate.ApellidoEmpleado != null ? PermissionToUpdate.ApellidoEmpleado : newPermission.ApellidoEmpleado;
                    newPermission.TipoPermisoId = PermissionToUpdate.TipoPermisoId > 0 ? PermissionToUpdate.TipoPermisoId : newPermission.TipoPermisoId;
                    newPermission.FechaPermiso = PermissionToUpdate.FechaPermiso != null ? PermissionToUpdate.FechaPermiso : newPermission.FechaPermiso;

                    _context.Permissions.Update(newPermission);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                    return newPermission;
                }
                catch (System.Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}