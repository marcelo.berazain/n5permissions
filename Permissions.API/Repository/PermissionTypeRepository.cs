using System.Linq;
using Permissions.Repository.Interfaces;
using Permissions.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using Permissions.Models;
using Microsoft.EntityFrameworkCore;

namespace Permissions.Repository
{
    public class PermissionTypeRepository : IPermissionTypeInterface
    {
        private readonly DataContext _context;
        public PermissionTypeRepository(DataContext context)
        {
            _context = context;
        }

        public int GetPermissionTypeList()
        {
            var counter = _context.PermissionTypes.Count();

            return counter;
        }

        public async Task<List<PermissionType>> GetPermissionTypeListAsync()
        {
            var permissionTypes = await _context.PermissionTypes
                .Distinct()
                .ToListAsync();

            return permissionTypes;
        }
    }
}