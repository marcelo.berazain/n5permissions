using System.Collections.Generic;
using System.Threading.Tasks;
using Permissions.Models;

namespace Permissions.Repository.Interfaces
{
    public interface IPermissionInterface
    {
        Task<Permission> GetPermissionById(int permissionId);
        Task<List<Permission>> GetPermissionList();
        Task<Permission> UpdatePermission(Permission PermissionToUpdate);
    }
}