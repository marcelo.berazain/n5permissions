using System.Collections.Generic;
using System.Threading.Tasks;
using Permissions.Models;

namespace Permissions.Repository.Interfaces
{
    public interface IPermissionTypeInterface
    {
        int GetPermissionTypeList();
        Task<List<PermissionType>> GetPermissionTypeListAsync();
    }
}