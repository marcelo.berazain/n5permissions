﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Permissions.Dtos;
using Permissions.Models;
using Permissions.Repository.Interfaces;

namespace Permissions.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PermissionController : ControllerBase
    {

        private readonly ILogger<PermissionController> _logger;
        private readonly IPermissionInterface _repo;
        private readonly IMapper _mapper;

        public PermissionController(ILogger<PermissionController> logger, IPermissionInterface repo, IMapper mapper)
        {
            _logger = logger;
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet("{permissionId}")]
        public async Task<IActionResult> RequestPermission(int permissionId)
        {
            try
            {
                var permissionFromRepo = await _repo.GetPermissionById(permissionId);
                var permissionToReturn = _mapper.Map<PermissionToListDto>(permissionFromRepo);

                return Ok(permissionToReturn);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Fallo al obtener permiso");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetPermissions()
        {
            try
            {
                var permissionListFromRepo = await _repo.GetPermissionList();
                var permissionListToReturn = _mapper.Map<List<PermissionToListDto>>(permissionListFromRepo);

                return Ok(permissionListToReturn);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Fallo al listar permisos");
            }
        }

        [HttpPut]
        public async Task<IActionResult> ModifyPermission(Permission PermissionToUpdate)
        {
            try
            {
                if (PermissionToUpdate.Id == 0)
                {
                    return BadRequest("Se requiere el id a editar");
                }

                var permissionUpdated = await _repo.UpdatePermission(PermissionToUpdate);

                var permissionFromRepo = await _repo.GetPermissionById(permissionUpdated.Id);
                var permissionToReturn = _mapper.Map<PermissionToListDto>(permissionFromRepo);

                return Ok(permissionToReturn);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Fallo al editar permiso");
            }
        }
    }
}
