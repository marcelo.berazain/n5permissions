﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Permissions.Repository.Interfaces;

namespace Permissions.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PermissionTypeController : ControllerBase
    {

        private readonly ILogger<PermissionTypeController> _logger;
        private readonly IPermissionTypeInterface _repo;
        private readonly IMapper _mapper;

        public PermissionTypeController(ILogger<PermissionTypeController> logger, IPermissionTypeInterface repo, IMapper mapper)
        {
            _logger = logger;
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetPermissions()
        {
            try
            {
                var permissionTypeListFromRepo = await _repo.GetPermissionTypeListAsync();

                return Ok(permissionTypeListFromRepo);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Fallo al listar tipos de permiso");
            }
        }
    }
}
