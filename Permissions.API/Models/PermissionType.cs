using System.Collections.Generic;

namespace Permissions.Models
{
    public class PermissionType
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public virtual ICollection<Permission> Permisos{ get; set; }
    }
}