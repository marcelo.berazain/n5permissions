using System;

namespace Permissions.Dtos
{
    public class PermissionToListDto
    {
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string ApellidoEmpleado { get; set; }
        public int TipoPermisoId { get; set; }
        public string DescripcionPermiso { get; set; }
        public string FechaPermiso { get; set; }
    }
}