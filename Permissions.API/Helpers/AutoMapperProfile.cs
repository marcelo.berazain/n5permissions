using AutoMapper;
using Permissions.Dtos;
using Permissions.Models;

namespace Permissions.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Permission, PermissionToListDto>()
                .ForMember(dest => dest.DescripcionPermiso, opt => opt.MapFrom(src => src.TipoPermiso.Descripcion))
                .ForMember(dest => dest.FechaPermiso, opt => opt.MapFrom(src => src.FechaPermiso.Day + "/" + src.FechaPermiso.Month + "/" + src.FechaPermiso.Year));
        }
    }
}